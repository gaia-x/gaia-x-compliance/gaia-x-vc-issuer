// Define "require"
import { createRequire } from "module";
const require = createRequire(import.meta.url);

import { getDID } from './did.js';
// import { getTrustChain } from './trust_chain.js';
import config from './config.js';
import { generateVC } from './membership.js';

const express = require('express');
const app = express();

app.get('/health', (req, res) => {
    res.json({ status: "ok" });
});

// app.get('/issuer/trust_chain.jsonld', async (req, res) => {
//     let vc = await getTrustChain(config, req);
//     res.json(vc);
// });

app.get('/issuer/did.json', async (req, res) => {
    let did = await getDID(config, req);
    res.json(did);
});

app.get('/membership/pierre.gronlier@gaia-x.eu', async (req, res) => {
    let email = 'pierre.gronlier@gaia-x.eu';
    let vc = await generateVC(config, req, email);
    res.json(vc);
});

app.get('/issuer/fullchain.cer', async (req, res) => {
    res.download(config.cert.domain.certificateFile);
});

app.listen(config.server.port, () => {
    console.log(`Listening at http://localhost:${config.server.port}`)
});

export const server = app;
