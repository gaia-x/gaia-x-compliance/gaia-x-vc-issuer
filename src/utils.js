import { strict as assert } from 'assert';
import { base58btc } from "multiformats/bases/base58"
import { createPrivateKey, createPublicKey } from 'crypto';

// Define "require"
import { createRequire } from "module";
const require = createRequire(import.meta.url);

const jose = require('jose')

// from https://github.com/digitalbazaar/ed25519-verification-key-2020/blob/v3.2.0/lib/Ed25519VerificationKey2020.js
// multicodec ed25519-pub header as varint
const MULTICODEC_ED25519_PUB_HEADER = new Uint8Array([0xed, 0x01]);
// multicodec ed25519-priv header as varint
const MULTICODEC_ED25519_PRIV_HEADER = new Uint8Array([0x80, 0x26]);

export function convertToMultibase(key) {
    assert(key.asymmetricKeyType === 'ed25519');
    let jwk = key.export({ format: 'jwk' });
    if (key.type === 'public') {
        return base58btc.encode(Buffer.concat([MULTICODEC_ED25519_PUB_HEADER, Buffer.from(jwk.x, 'base64url')]));
    } else { // 'private'
        return base58btc.encode(Buffer.concat([MULTICODEC_ED25519_PRIV_HEADER, Buffer.from(jwk.d, 'base64url'), Buffer.from(jwk.x, 'base64url')]));
    }
}

async function verifyKeypair(publicKey, privateKey) {
    const jwt = await new jose.SignJWT({
        subject: "uuid",
    }).setProtectedHeader({ alg: "EdDSA" })
        .setExpirationTime("2h")
        .sign(privateKey);
    await jose.jwtVerify(jwt, publicKey);
}

export async function verifyWithDer(publicKeyMultibase, privateKeyMultibase) {
    // https://stackoverflow.com/questions/68612396/sign-and-verify-jws-json-web-signature-with-ed25519-keypair
    let publicKey = base58btc.decode(publicKeyMultibase);
    publicKey = publicKey.slice(MULTICODEC_ED25519_PUB_HEADER.length);
    publicKey = createPublicKey({
        key: Buffer.concat([Buffer.from("302a300506032b6570032100", "hex"), publicKey]),
        format: "der", type: "spki",
    });

    let privateKey = base58btc.decode(privateKeyMultibase);
    privateKey = privateKey.slice(MULTICODEC_ED25519_PRIV_HEADER.length);
    privateKey = createPrivateKey({
        key: Buffer.concat([Buffer.from("302e020100300506032b657004220420", "hex"), privateKey.slice(0, 32)]),
        format: "der", type: "pkcs8",
    })
    await verifyKeypair(publicKey, privateKey);
}

export async function verifyWithJwk(publicKeyMultibase, privateKeyMultibase) {
    // https://stackoverflow.com/questions/68612396/sign-and-verify-jws-json-web-signature-with-ed25519-keypair
    let publicKey = Buffer.from(base58btc.decode(publicKeyMultibase));
    publicKey = publicKey.slice(MULTICODEC_ED25519_PUB_HEADER.length);
    publicKey = createPublicKey({
        key: { crv: "Ed25519", kty: "OKP", x: publicKey.toString('base64url') }, format: 'jwk'
    });

    let privateKey = Buffer.from(base58btc.decode(privateKeyMultibase));
    privateKey = privateKey.slice(MULTICODEC_ED25519_PRIV_HEADER.length);
    privateKey = createPrivateKey({
        key: { crv: "Ed25519", kty: "OKP", x: "", d: privateKey.slice(0, 32).toString('base64url') }, format: 'jwk'
    });
    await verifyKeypair(publicKey, privateKey);
}
