process.on('unhandledRejection', function (reason) {
  throw reason;
});

// Define "require"

import { createRequire } from "module";
const require = createRequire(import.meta.url);
const jp = require('jsonpath');

//Require the dev-dependencies
let chai = require('chai');
chai.use(require('chai-http'));
chai.use(require('chai-string'));
import { server } from '../src/server.js';
let should = chai.should();

const axios = require('axios');

let jsigs = require('jsonld-signatures');
const { purposes: { AssertionProofPurpose } } = jsigs;
let { Ed25519Signature2020 } = require('@digitalbazaar/ed25519-signature-2020');

import { myDocumentLoader } from '../src/membership.js';

//Our parent block
describe('Server', () => {
  // beforeEach((done) => { //Before each test we empty the database
  //     Book.remove({}, (err) => {
  //        done();
  //     });
  // });
  /*
    * Test the /GET route
    */
  describe('/GET health', () => {
    it('simple health check', async () => {
      let res = await chai.request(server).get('/health');
      res.should.have.status(200);
      res.should.be.json;
      res.body.should.be.a('object');
      res.body.should.have.property('status');
      res.body.status.should.eql('ok');
    });
  });

  describe('/GET did:web:issuer', () => {
    it('get Gaia-X DID', async () => {
      let res = await chai.request(server).get('/issuer/did.json');
      res.should.have.status(200);
      res.should.be.json;
      res.body.should.be.a('object');
      res.body.should.have.property('id');
      res.body.should.have.property('assertionMethod');
      res.body.should.have.property('verificationMethod');
      res.body.assertionMethod.should.have.length(res.body.verificationMethod.length);
      res.body.verificationMethod.should.have.length(3);
      for (let method of res.body.verificationMethod) {
        (method.id.split('#'))[1].should.startsWith(method.type);
        if ((method.id.split('#'))[1] == "Ed25519VerificationKey2020") {
          method.should.have.property('publicKeyMultibase');
          method.publicKeyMultibase.should.length(48);
        }
        if ((method.id.split('#'))[1] == "JsonWebKey2020-Ed25519") {
          method.should.have.property('publicKeyJwk');
          method.publicKeyJwk.should.not.have.property('d');
        }
        if ((method.id.split('#'))[1] == "JsonWebKey2020-RSA") {
          method.should.have.property('publicKeyJwk');
        }
      }
      res.body.should.have.property('service');
      res.body.service.should.be.a('array');
      let rootserver = res.req.protocol + "//" + res.request.host;
      for (let svc of res.body.service) {
        if (svc.serviceEndpoint.startsWith(rootserver)) {
          let svcres = await chai.request(server).get(svc.serviceEndpoint.replace(rootserver, ""));
          svcres.should.have.status(200);
          if (svc.type === 'X509Certificate')
            svcres.type.should.be.oneOf(['application/pkix-cert', 'application/x-x509-ca-cert']);
        } else {
          let svcres = await axios.get(svc.serviceEndpoint);
          svcres.should.have.status(200);
        }
      }
    });
  });

  describe('/GET /membership/pierre.gronlier@gaia-x.eu', () => {
    it('get membership VC', async () => {
      var requester = chai.request(server).keepOpen()
      let res = await requester.get('/membership/pierre.gronlier@gaia-x.eu');
      res.should.have.status(200);
      res.should.be.json;
      res.body.should.be.a('object');
      res.body.should.have.property('proof');

      let proof = jp.query(res.body, '$..[?(@.type=="Ed25519Signature2020")]');
      proof.should.length.above(0);
      proof = proof[0];
      proof.should.have.property('type');
      proof.type.should.eq('Ed25519Signature2020');
      proof.should.have.property('verificationMethod');

      let check_vc = await jsigs.verify(res.body, {
        suite: new Ed25519Signature2020(),
        purpose: new AssertionProofPurpose(),
        documentLoader: myDocumentLoader
      });

      console.log(check_vc);
      requester.close();
      check_vc.verified.should.eq(true);
    });
  })
});
